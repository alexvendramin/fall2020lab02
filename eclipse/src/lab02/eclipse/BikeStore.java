//Alex Vendramin - 1933413
package lab02.eclipse;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] b = new Bicycle[4];
		b[0] = new Bicycle("Focus", 6, 40.0);
		b[1] = new Bicycle("Trek", 4, 30.0);
		b[2] = new Bicycle("Felt", 3, 20.0);
		b[3] = new Bicycle("BMC", 0, 10.0);
		for(int i = 0; i < b.length; i++) {
			System.out.println(b[i].toString());
		}
	}

}
