//Alex Vendramin - 1933413
package lab02.eclipse;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturer, int numberGears, double maxSpeed){
	this.manufacturer = manufacturer;
	this.numberGears = numberGears;
	this.maxSpeed = maxSpeed;
	}
	
	public String getManufacturer(){
		return this.manufacturer;
	}
	
	public int getNumberGears() {
		return this.numberGears;
	}
	
	public double getMaxSpeed() {
		return this.maxSpeed;
	}
	
	public String toString() {
		return "manufacturer = " + this.manufacturer + "; number of gears = " + this.numberGears + "; max speed = " + this.maxSpeed;
	}
}
